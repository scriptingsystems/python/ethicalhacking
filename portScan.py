#!/usr/bin/python3
import socket
import sys

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
hostIp=str(sys.argv[1])
hostPort=int(sys.argv[2])

def portScanner(ip, port):
    if s.connect_ex((ip, port)):
        print("The port %d is closed" % port)
    else:
        print("The port %d is opened" % port)

portScanner(hostIp,hostPort)
