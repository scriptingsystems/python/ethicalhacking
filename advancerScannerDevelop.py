#!/usr/bin/python3
import optparse
import sys
import socket
from colorama import Fore

def isPortInUse(host:str, ip:int) -> bool:
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
      return s.connect_ex((host,ip)) == 0

def main():
    parser = optparse.OptionParser('Usage of program -H <Target Host> -p <Target Port>')
    parser.add_option('-H',dest='tgtHost',type='string',help='specify target host')
    parser.add_option('-p',dest='tgtPorts',type='string',help='specify target ports separate by comma')
    (options, args) = parser.parse_args()
    tgtHost = options.tgtHost
    tgtPorts = str(options.tgtPorts).split(',')
    if (not tgtHost ) or (not tgtPorts):
        print(parser.usage)
        sys.exit(1)
    for port in tgtPorts:
        if isPortInUse(tgtHost, int(port)):
            print(Fore.BLUE + f"[+] The port {port} is opened")
        else:
            print(Fore.RED + f"[-] The port {port} is closed")
    

if __name__ == '__main__':
    main()    