#!/usr/bin/python3
import socket
from colorama import Fore

hostIP = input("Enter The host to Scanner: ")

def isPortInUse(ip:str, port: int) -> bool:
    with socket.socket(socket.AF_INET,socket.SOCK_STREAM) as s:
        return s.connect_ex((ip,port)) == 0

for hostPort in range(1,1000):
    if isPortInUse(hostIP,hostPort):
        print(Fore.BLUE + f"The port {hostPort} is opened")