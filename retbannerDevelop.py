#!/usr/bin/python3
import optparse
import sys
import socket

def isPortInUse(hostIp:str, hostPort:int):
    try:
        s = socket.socket()
        s.connect((hostIp, hostPort))
        s.settimeout(2)
        banner = s.recv(1024)
        return banner.decode().strip()
    except Exception as e:
        return None
    finally:
        s.close()
        
def main():
    parser = optparse.OptionParser("Usege of script: -H <Target host>")
    parser.add_option("-H","--host", dest="tgtHostIp",type="string",help="Specify target host")
    (options, args) = parser.parse_args()
    tgtHostIp = options.tgtHostIp
    if (not tgtHostIp):
        print(parser.usage)
        sys.exit(1)
    for tgtHostPort in range(1,1024):
        banner = isPortInUse(tgtHostIp,tgtHostPort)
        if banner:
            print(f"The port {tgtHostPort} is opened. Banner: {banner}")

if __name__ == "__main__":
    main()
